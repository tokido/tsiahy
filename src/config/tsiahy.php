<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Application Locales
    |--------------------------------------------------------------------------
    |
    | Contains an array with the applications locales default sources.
    |
    */
    'status'    => [
        'pending',
        'approved',
        'denied'
    ],

    'state'    => [
        'snapshot',
        'active'
    ]
];
