<?php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTsiahyMementoTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tsiahy_snapshots', function (Blueprint $table) {
            $table->increments('id');
            $table->string('serialized_data');
            $table->integer('snapshotable_id');
            $table->string('snapshotable_type');
            $table->timestamps();
        });

        DB::statement("ALTER TABLE `tsiahy_snapshots` CHANGE `serialized_data` `serialized_data` BLOB  NULL");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tsiahy_snapshots');
    }
}
