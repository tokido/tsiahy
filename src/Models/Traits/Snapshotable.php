<?php

namespace Tokido\Tsiahy\Models\Traits;

use App;
use Tokido\Tsiahy\Tsiahy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;

trait Snapshotable
{
    /**
    * Who inherits those traits will be an Originator
    */
    public static function bootSnapshotable()
    {

    }

    public function snapshots()
    {
        return $this->morphMany('Tokido\Tsiahy\Models\TsiahySnapshot', 'snapshotable');
    }
}
