<?php

namespace Tokido\Tsiahy;

use Illuminate\Support\ServiceProvider;

class TsiahyServiceProvider extends ServiceProvider
{
    /**
     * Register the boot
     */
    public function boot()
    {
        $modulePath     = realpath(__DIR__);
        
        $config_path    = $modulePath . '/config/tsiahy.php';
        $route_path     = $modulePath . '/routes/tsiahy.php';
        $migration_path = $modulePath . '/migrations/';
        $views_path     = $modulePath . '/views';

        $this->mergeConfigFrom($config_path, 'tsiahy');

        $this->publishes([
            $config_path => config_path('tsiahy.php')
        ]);

        $this->loadRoutesFrom($route_path);

        $this->loadMigrationsFrom($migration_path);

        $this->loadViewsFrom($views_path, 'tsiahy');
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        
        $this->app->singleton(Tsiahy::class, function () {
            return new Tsiahy();
        });

        $this->app->alias(Tsiahy::class, 'tsiahy');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [Tsiahy::class];
    }
}
