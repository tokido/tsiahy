<?php 

namespace Tokido\Tsiahy\Models;

Use Illuminate\Database\Eloquent\Model;

class TsiahySnapshot extends Model
{
    /*
    * Tsiahy SnapShot work as Memento
    *
    */
    protected $fillable = [
        'serialized_data',
        'snapshotable_id',
        'snapshotable_type'
    ];
}